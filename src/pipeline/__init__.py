from pipeline.node1 import Node1
from pipeline.node2 import Node2

class Pipeline:
    def __init__(self, config):
        self.nodes = []
        for n in config:
            if n['name'] == 'Node1':
                self.nodes.append(Node1(**n['param']))
            elif n['name'] == 'Node2':
                self.nodes.append(Node2(**n['param']))
            else:
                print(f"Unrecognized node {n['name']}")

    def apply(self, d):
        for node in self.nodes:
            d = node.apply(d)
        return d