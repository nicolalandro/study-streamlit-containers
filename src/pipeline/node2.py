class Node2:
    def __init__(self, in_field, out_field):
        self.in_field = in_field
        self.out_field = out_field

    def apply(self, d):
        d[self.out_field] = f"You copy [{d[self.in_field]}]"
        return d