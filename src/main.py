import json

import streamlit as st

import streamlit_authenticator as stauth
import yaml

from streamlit_elements import elements, mui, html, editor, lazy, dashboard

from pipeline import Pipeline
from pipeline.node1 import Node1

st.set_page_config(layout="wide")

##############################################
#
# Auth Config
#
##############################################

with open('config.yml') as file:
    config = yaml.load(file, Loader=yaml.SafeLoader)

passwords = []
for key, value in config['credentials']['usernames'].items():
    passwords.append(str(value['password']))


hashed_passwords = stauth.Hasher(passwords).generate()
i = 0
for key, value in config['credentials']['usernames'].items():
    config['credentials']['usernames'][key]['password'] = hashed_passwords[i]
    i = i + 1


authenticator = stauth.Authenticate(
    config['credentials'],
    config['cookie']['name'],
    config['cookie']['key'],
    config['cookie']['expiry_days'],
    config['preauthorized']
)

##############################################
#
# Session variable
#
##############################################
if "content" not in st.session_state:
    st.session_state.content = "[]"
if "run" not in st.session_state:
    st.session_state.run = ""


##############################################
#
# GUI Main
#
##############################################

st.title('Manage Pipeline App (jsmith, 123)')
name, authentication_status, username = authenticator.login('Login', 'main')
if authentication_status:
    with st.sidebar:
        authenticator.logout('Logout', 'main')
        st.write(f'Welcome *{name}*')
        # upload_ui = st.button('Upload UI From File')
        node = st.radio(
            "Select Node 👇",
            ["Node1", "Node2"],
        )
        add_node = st.button('Add Selected Node')
        if add_node:
            if node == 'Node1':
                data = json.loads(st.session_state.content)
                data.append({
                    "name": "Node1",
                    "param": {}
                })
                st.session_state.content = json.dumps(data)
            elif node == 'Node2':
                data = json.loads(st.session_state.content)
                data.append({
                    "name": "Node2",
                    "param": {
                        'in_field': 'node1',
                        'out_field': 'node2'
                    }
                })
                st.session_state.content = json.dumps(data)
        
        if st.button('Run Pipeline'):
            data = json.loads(st.session_state.content)
            st.session_state.run = Pipeline(data).apply({})
            

    layout = [
        dashboard.Item("first_item", 0, 0, 6, 4),
        dashboard.Item("second_item", 6, 0, 6, 2),
        dashboard.Item("third_item", 6, 2, 6, 2)
    ]
    with elements("dashboard"):
        with dashboard.Grid(layout):
            with mui.Card(key="first_item"):
                mui.Typography("Pipeline UI")
                data = json.loads(st.session_state.content)
                for i, n in enumerate(data):
                    if n['name'] == 'Node1':
                        mui.Typography("Node 1 does not have options")
                    elif n['name'] == 'Node2':
                        mui.Typography("Node 2 options:")
                        def handle_in_field_change(event):
                            text = event.target.value
                            data =json.loads(st.session_state.content)
                            data[i]['param']['in_field'] = text
                            st.session_state.content = json.dumps(data)
                        mui.TextField(label="in_field", onChange=handle_in_field_change)
                        def handle_out_field_change(event):
                            text = event.target.value
                            data =json.loads(st.session_state.content)
                            data[i]['param']['out_field'] = text
                            st.session_state.content = json.dumps(data)
                        mui.TextField(label="out_field", onChange=handle_out_field_change)

                
            with mui.Card(key="second_item"):
                mui.Typography(f"Pipeline file : {st.session_state.content}")

                with elements(f"config_editor"):
                    def update_content(value):
                        st.session_state.content = value
                    # editor.Monaco(
                    #     defaultValue=st.session_state.content,
                    #     onChange=lazy(update_content)
                    # )
    
            with mui.Card(key="third_item"):
                mui.Typography("Run result")
                mui.Typography(json.dumps(st.session_state.run))
elif authentication_status == False:
    st.error('Username/password is incorrect')
elif authentication_status == None:
    st.warning('Please enter your username and password')
