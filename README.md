# Study Streamlit container
To study streamlit container I made this simple pipeline creator.

## How to run
```
docker-compose up
# go to localhost:8000
```

# References
* docker, docker compose
* python
* streamlit
* [streamlit Containers](https://docs.streamlit.io/library/api-reference/layout)
  * [Streamlit Elements](https://github.com/okld/streamlit-elements)
  * [Streamlit Pydatic](https://github.com/lukasmasuch/streamlit-pydantic)